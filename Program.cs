﻿using System;
using System.Threading;

namespace wdmtechnicaltest2022
{
    class MainClass
    {

        public static void Main(string[] args)
        {
            //greet the user
            Console.WriteLine("Good Morning " + System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            Console.WriteLine("Let's do some maths");
            Console.WriteLine("I can add (+), subtract (-), multiply (*), and divide (/) whole and decimal numbers. Press Enter when you want the answer.");
            Console.WriteLine("Please note: I calculate from left to right, that's just the way I like to do things :D");
            // read the user input
            Console.WriteLine("What would you like me to calculate?");
            var UserInput = Console.ReadLine();
            try
            {
                //remove spaces to clean up the input
                UserInput = UserInput.Replace(" ", "");
                //add a single space back in for each supported operator to create a delimeter
                UserInput = UserInput.Replace("+", " + ");
                UserInput = UserInput.Replace("-", " - ");
                UserInput = UserInput.Replace("*", " * ");
                UserInput = UserInput.Replace("/", " / ");
                // split the string on spaces
                string[] calcstring = UserInput.Split(new Char[] { ' ' });
                //Initialise the subtotal with the first value.
                double subTotal = 0;
                if ((calcstring[0] == ""))     //if the calc starts with an operator then the first number is zero.
                {
                    subTotal = 0;
                }
                else if (double.TryParse(calcstring[0], out double ln))
                {
                    subTotal = double.Parse(calcstring[0]);
                }
                else
                {
                    throw new Exception("Invalid calculation: Unsupported character(s). The first number, aint a number!");
                }
                // work through the remaining elements 
                for (int i = 2; i < calcstring.Length; i++)
                {
                    
                    if (double.TryParse(calcstring[i], out double nextNum))
                    {
                        switch (calcstring[i - 1])
                        {
                            case "+": //add
                                subTotal += nextNum;
                                break;
                            case "-": //add
                                subTotal -= nextNum;
                                break;
                            case "*": //add
                                subTotal *= nextNum;
                                break;
                            case "/": //add
                                if (nextNum ==0)
                                {
                                    throw new Exception("Invalid calculation: Division by zero.");
                                }
                                subTotal /= nextNum;
                                break;
                        }
                    }
                    else if (!"+-*/".Contains(calcstring[i])) // else if not an operator then throw error
                    {
                        throw new Exception("Invalid calculation: Unsupported character(s). The value '" + calcstring[i] +  "' at position " + i + ", aint a number!");
                    }
                }
                Console.WriteLine("That one is easy: " + subTotal);
            }
            catch ( Exception e)
            {
                Console.WriteLine("I would do anything for maths, but I wont do that!");
                Console.WriteLine( e.Message);
            }
        }
    }
}
