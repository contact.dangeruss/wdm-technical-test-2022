# WDM Technical Test 2022

This is a one off project created in response to a techncial test. This is a console application which will interprit and execute a simple calculation given as a user-inputted string. 
The ‘simple’ calculation should supports following operations from left to right without consideration to operator precedence: Addition, subtraction, multiplication, and division.
Only whole and decimal numbers are be supported.
